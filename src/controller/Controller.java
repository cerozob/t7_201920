package controller;

import java.util.Scanner;


import model.logic.UberLogic;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private UberLogic modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new UberLogic();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					System.out.println("Cargar Grafo");
					modelo.loadGraph("./data/bogota_arcos.txt","./data/bogota_vertices.txt");
					break;
				case 2:
					break;

				case 3:
					break;

				case 4:
					break;

				case 5: 
					break;	
					
				case 6: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;	

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
