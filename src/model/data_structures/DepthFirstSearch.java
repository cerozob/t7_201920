package model.data_structures;

import java.util.Iterator;

public class DepthFirstSearch {
	
	private boolean[] marked;
	private int count;
	
	public DepthFirstSearch(Graph G, int s)
	{
		marked = new boolean[G.V()];
		dfs(G, s);
	}
	private void dfs(Graph G, int v)
	{
		marked[v] = true;
		count++;
		Iterator<WeighedEdge> iter = (Iterator<WeighedEdge>) G.adj(v).iterator();
		while(iter.hasNext())
		{
			int w = iter.next().other();
			if (!marked[w]) dfs(G, w);
		}
			
	}
	public boolean marked(int w)
	{ return marked[w]; }
	public int count()
	{ return count; }
}
