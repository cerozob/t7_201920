package model.data_structures;

import java.util.Iterator;

public  class LinearProbingHashST< K extends Comparable<K>, V> implements IHashTable < K, V>
{
	private int N;
	private int M; 
	private V[] vals;
	private K[] keys;
	private int contador;

	@SuppressWarnings("unchecked")
	public LinearProbingHashST(int tam)
	{
		contador = 0;
		keys = (K[]) new Comparable[tam];
		vals = (V[]) new Object[tam];
		M = tam;
	}

	private int hash(K key)
	{	return (key.hashCode() & 0x7fffffff) % M;	}

	public void put(K key, V val) 
	{
		double n = N;
		double m = M;
		if (n/m >= 0.75)	resize(M*2);
		int i;
		for( i = hash(key); keys[i]!=null; i = (i+1) % M)
			if (keys[i].equals(key)) 
			{ 
				vals[i] = val; 
				return;
			}
		keys[i] = key;
		vals[i] = val;
		N++;
	}


	public  void putInSet(K key, V val)
	{
		if (N/M >= 0.75)	resize(M*2);
		int i;
		for( i = hash(key); keys[i]!=null && !keys[i].equals(key); i = (i+1) % M) {}
		for(int l = M; l > i; i--)
			keys[l] = keys[l+1]; //corrimiento de keys
		for(int l = M; l > i; i--)
			vals[l] = vals[l+1]; //corrimiento de vals  **No sé si hay que hacerlo
		keys[i] = key;
		vals[i] = val;
		N++;
	}

	public  V get(K key)
	{
		for (int i = hash(key); keys[i] != null; i = (i + 1) % M)
			if (keys[i].equals(key))
				return vals[i];
		return null;
	}

	public V delete(K key) {
		V temp = vals[hash(key)];
		V retornar = get(key);
		if(temp == null)
		for (int i = hash(key); i < M; i++)
			vals[i] = vals[i-1];
		return retornar;
	}


	public Iterator<V> getSet(K key) 
	{
		LinkedList<V> rta = new LinkedList<V>();
		for (int i = hash(key); keys[i] != null; i++)
			if(keys[i].equals(key))
				rta.append(vals[i]);
		return (Iterator<V>)rta.iterator();
	}


	public Iterator<V> deleteSet(K key) {
		LinkedList<V> rta = new LinkedList<V>();
		for (int i = hash(key); keys[i] != null; i++)
			if(keys[i].equals(key))
			{
				rta.append(vals[i]);
				for (int n = hash(key); n < M; n++)
					vals[n] = vals[n-1];
			}
		return (Iterator<V>)rta.iterator();
	}


	public Iterator<K> keys() {
		LinkedList<K> rta = new LinkedList<K>();
		for (int i = 0; i<M; i++)
			if(keys[i]!=null)
				rta.append(keys[i]);
		return (Iterator<K>) rta.iterator();
	}

	private void resize(int nSize)
	{
		LinearProbingHashST<K, V> temp = new LinearProbingHashST<K,V>(nSize);
		for (int i = 0; i < M; i++)
			if (keys[i] != null)
				temp.put(keys[i], vals[i]);
		keys = temp.keys;
		vals = temp.vals;
		M = temp.M;
		contador++;
	}

	public int size() 
	{	return N;	}
	
	public boolean isEmpty() 
	{	return N == 0;	}
	
	public boolean contains(K key) 
	{	return (get(key) != null);	}

	public int contador()
	{	return contador;	}

	public double darFactorCarga()
	{	double n = N;
	double m = M;
	return n/m;		}

	public int getM() {
		
		return M;
	}

}
