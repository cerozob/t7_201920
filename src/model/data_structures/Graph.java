package model.data_structures;

import java.util.Iterator;

public class Graph<V> implements IGrafo<V> {
	
	private IHashTable<Integer,V> vertices;
    private final int V;
    private int E;
    private Bag<WeighedEdge>[] adj;
    
    /**
     * Initializes an empty edge-weighted graph with {@code V} vertices and 0 edges.
     *
     * @param  V the number of vertices
     * @throws IllegalArgumentException if {@code V < 0}
     */
    public Graph(int V) {
        if (V < 0) throw new IllegalArgumentException("Number of vertices must be nonnegative");
        this.V = V;
        this.E = 0;
        vertices = new SeparateChainingHashST<Integer, V>(V);
        adj = (Bag<WeighedEdge>[]) new Bag[V];
        for (int v = 0; v < V; v++) {
            adj[v] = new Bag<WeighedEdge>();
        }
    }
    
    /**
     * Returns the number of vertices in this edge-weighted graph.
     *
     * @return the number of vertices in this edge-weighted graph
     */
    public int V() {
        return V;
    }

    /**
     * Returns the number of edges in this edge-weighted graph.
     *
     * @return the number of edges in this edge-weighted graph
     */
    public int E() {
        return E;
    }

    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(int v) {
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }

    /**
     * Adds the undirected edge {@code e} to this edge-weighted graph.
     *
     * @param  e the edge
     * @throws IllegalArgumentException unless both endpoints are between {@code 0} and {@code V-1}
     */
    public void addEdge(WeighedEdge e) {
        int v = e.either();
        int w = e.other();
        validateVertex(v);
        validateVertex(w);
        adj[v].add(e);
        adj[w].add(e);
        E++;
    }

    /**
     * Returns the edges incident on vertex {@code v}.
     *
     * @param  v the vertex
     * @return the edges incident on vertex {@code v} as an Iterable
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public Iterable<WeighedEdge> adj(int v) {
        validateVertex(v);
        return adj[v];
    }

    /**
     * Returns the degree of vertex {@code v}.
     *
     * @param  v the vertex
     * @return the degree of vertex {@code v}               
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public int degree(int v) {
        validateVertex(v);
        return adj[v].size();
    }

    /**
     * Returns all edges in this edge-weighted graph.
     * To iterate over the edges in this edge-weighted graph, use foreach notation:
     * {@code for (Edge e : G.edges())}.
     *
     * @return all edges in this edge-weighted graph, as an iterable
     */
    public Iterable<WeighedEdge> edges() {
        Bag<WeighedEdge> list = new Bag<WeighedEdge>();
        for (int v = 0; v < V; v++) {
            int selfLoops = 0;
            for (WeighedEdge e : adj(v)) {
                if (e.other() > v) {
                    list.add(e);
                }
                // add only one copy of each self loop (self loops will be consecutive)
                else if (e.other() == v) {
                    if (selfLoops % 2 == 0) list.add(e);
                    selfLoops++;
                }
            }
        }
        return list;
    }

	@Override
	public void addEdge(int idVertexIni, int idVertexFin, double cost) {
		WeighedEdge e = new WeighedEdge(idVertexIni, idVertexFin, cost); 
		addEdge(e);
	}

	@Override
	public V getInfoVertex(int idVertex) {
		return vertices.get(idVertex);
	}

	@Override
	public void setInfoVertex(int idVertex, V infoVertex) {
		if (vertices.contains(idVertex)) 
		{
			vertices.delete(idVertex);
			vertices.put(idVertex, infoVertex);
		}
		else 
			vertices.put(idVertex, infoVertex);
	}

	@Override
	public double getCostArc(int idVertexIni, int idVertexFin) {
		Iterator<WeighedEdge> iter = edges().iterator();
		while(iter.hasNext())
		{
			WeighedEdge e = iter.next();
			if(e.either() == idVertexIni && e.other() == idVertexFin || e.either() == idVertexFin && e.other() == idVertexIni)
			{
				return e.weight();
			}
		}
		return 0;
	}

	@Override
	public void setCostArc(int idVertexIni, int idVertexFin, double cost) {
		Iterator<WeighedEdge> iter = edges().iterator();
		int edges = 0;
		while(iter.hasNext()&& edges != 2)
		{
			WeighedEdge e = iter.next();
			if(e.either() == idVertexIni && e.other() == idVertexFin || e.either() == idVertexFin && e.other() == idVertexIni)
			{
				e.setWeight(cost);
				edges++;
			}
			
		}
	}

	@Override
	public void addVertex(int idVertex,V infoVertex)
	{
		if(vertices.get(idVertex) == null)
		{
			vertices.put(idVertex, infoVertex);
		}
	}
	
	@Override
	public void uncheck() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dfs(int s) {
		DepthFirstSearch dfs = new DepthFirstSearch(this, s);
	}

	@Override
	public int cc() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Iterable<Integer> getCC(int idVertex) {
		// TODO Auto-generated method stub
		return null;
	}

}


//Copyright � 2000�2019, Robert Sedgewick and Kevin Wayne.
//Last updated: Sat Nov 16 07:05:36 EST 2019.