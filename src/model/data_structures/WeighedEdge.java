package model.data_structures;

public class WeighedEdge implements Comparable<WeighedEdge> { 

    private final int v;
    private final int w;
    private double weight;

    public WeighedEdge(int v, int w, double weight) {
        if (v < 0) throw new IllegalArgumentException("vertex index must be a nonnegative integer");
        if (w < 0) throw new IllegalArgumentException("vertex index must be a nonnegative integer");
        if (Double.isNaN(weight)) throw new IllegalArgumentException("Weight is NaN");
        this.v = v;
        this.w = w;
        this.weight = weight;
    }

    public double weight() {
        return weight;
    }
    
    public void setWeight(double pWeight)
    {
    	weight = pWeight;
    }
   
    public int either() {
        return v;
    }

  
    public int other() {
       return w;

    }

    @Override
    public int compareTo(WeighedEdge that) {
    	if(weight > that.weight())
    	{
    		return 1;
    	}
    	else if(weight < that.weight())
    	{
    		return -1;
    	}
    	else
    	{
    		return 0;
    	}	
    }

}


//Copyright � 2000�2017, Robert Sedgewick and Kevin Wayne.
//Last updated: Fri Oct 20 12:50:46 EDT 2017.