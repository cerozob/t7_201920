package model.data_structures;

import java.util.Iterator;

public class SeparateChainingHashST<K extends Comparable<K> , V> implements IHashTable<K,V>
{
	private double n;
	private double m; 
	private int contador;
	private SequentialSearchST<K, V>[] st;

	@SuppressWarnings("unchecked")
	public SeparateChainingHashST (int tam)
	{
		m = tam;
		contador = 0;
		st = (SequentialSearchST<K,V>[]) new SequentialSearchST[tam];
		for (int i = 0; i < m; i++)
			st[i] = new SequentialSearchST<K,V>();
	}

	private int hash(K key) 
	{	return (key.hashCode() & 0x7fffffff) % (int)m;	}

	public void put(K key, V val)
	{
		if(key == null)
		{
			System.out.println("Llave invalida");
			return;
		}
		if(key != null && val != null)
		{
			if(n/m >= 5)
			{
				resize(2*((int)m));
			}

			int i = hash(key);
			if(!st[i].contains(key))
			{
				n++;
			}
			st[i].put(key,val);	
		}
		else if(key != null && val == null)
		{
			delete(key);
			return;
		}
	}

	public  V get(K key) {
		if(key != null)
		{
			{
				return st[hash(key)].get(key);
			}
		}
		return null;
	}

	public void putInSet(K key, V val)
	{	
		if(key != null && val != null)
		{
			if(n/m >= 5)
			{
				resize(2*(int)m);
			}
			st[hash(key)].putInSet(key, val);
		}
		n++;
	}

	public V delete(K key)
	{
		V rta = null;
		if(key != null)
		{
			rta = st[hash(key)].get(key);
			st[hash(key)].delete(key);	
			if(n/m <= 2)
			{
				resize((int)(m/2));
			}
			
		}
		return rta;
	}

	private void resize(int nSize)
	{
		SeparateChainingHashST<K, V> temp = new SeparateChainingHashST<K,V>(nSize);

		for (int i = 0; i < m; i++)
		{
			Iterator<K> iter = st[i].keys();
			while(iter.hasNext())
			{
				K key = iter.next();
				temp.put(key,st[i].get(key));
			}
		}
		n = temp.n;
		m = temp.m;
		st = temp.st;
		contador++;
	}


	public Iterator<V> getSet(K key){
		return st[hash(key)].getSet(key);
	}
	public Iterator<V> deleteSet(K key){
		return st[hash(key)].deleteSet(key);
	}
	public Iterator<K> keys(){
		LinkedList<K> lista = new LinkedList<K>();
		for (int i = 0; i < m; i++)
			if(st[i] != null) {
				Iterator<K> iter = st[i].keys();
				while(iter.hasNext()) 
				{	lista.append(iter.next());	} 
			}
		return (Iterator<K>)lista.iterator();
	}
	@Override
	public int size() {
		return (int)n;
	}
	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	@Override
	public boolean contains(K key) {
		return (get(key) != null);
	}

	public int contador()
	{
		return contador;
	}

	public double darFactorCarga()
	{
		return n/m;
	}

	public int getM() {

		return (int)m;
	}


}
