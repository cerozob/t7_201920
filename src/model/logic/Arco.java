package model.logic;

public class Arco {
	
	private Vertice a;
	
	private Vertice b;
	
	private double costo;
	
	public Arco(Vertice pA, Vertice pB, double pCosto)
	{
		a = pA;
		b = pB;
		costo = pCosto;
	}

	public Vertice getA() {
		return a;
	}

	public Vertice getB() {
		return b;
	}

	public double getCosto() {
		return costo;
	}
	
}
