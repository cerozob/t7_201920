package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.util.Iterator;

import model.data_structures.*;
import com.google.gson.*;

/**
 * Definicion del modelo del mundo
 *
 */
public class UberLogic {

	/**
	 * Atributos del modelo del mundo
	 */

	/**
	 *Ayuda: Puede usar como llave un String que sea la concatenaci�n de trimestre,
	 *sourceid y dstid separados por �-�.
	 */

	public final int MAX_CONSOLA = 20;

	private IHashTable<String,Vertice> sc; // datos de json

	private IGrafo<Vertice> graph;

	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public UberLogic()
	{
		sc = new SeparateChainingHashST<String ,Vertice>(2);
	}

	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int getSCSize()
	{
		return sc.size();
	}

	public int getRehashesSC() {
		return sc.contador();
	}

	public int loadVertexTXT(String pRutaArchivo)
	{
		int contador = 0;
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(pRutaArchivo)));
			String actual;
			while((actual = br.readLine())!= null)
			{
				String[] linea = actual.split(";");
				Vertice v = new Vertice(Integer.parseInt(linea[0]),
						Double.parseDouble(linea[1]),
						Double.parseDouble(linea[2]),Integer.parseInt(linea[3]));
				sc.put(linea[3],v);
				contador++;
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contador;
	}

	public int loadLinksTXT(String pRutaArchivo, int numVertices)
	{
		graph = new Graph<Vertice>(numVertices);
		int contador = 0;
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(pRutaArchivo)));
			String actual;
			while((actual = br.readLine())!= null)
			{
				String[] linea = actual.split(" ");

				Vertice pA = sc.get(linea[0]);
				for(int i = 1;i<linea.length;i++)
				{
					Vertice pB = sc.get(linea[i]);
					double pCosto = Haversine.distancia(pA, pB);
					Arco arco = new Arco(pA, pB, pCosto);
					graph.addEdge(arco.getA().getId(),arco.getB().getId(),arco.getCosto());
					contador++;
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contador;
	}

	public void loadGraph(String pRutaArchivoEdges,String pRutaArchivoVertices)
	{
		int v = loadVertexTXT(pRutaArchivoVertices);
		loadLinksTXT(pRutaArchivoEdges, v);
	}

	//clase comparable auxiliar
	public class ObjetoComparableAuxiliar implements Comparable<ObjetoComparableAuxiliar>{
		private int entero;
		private String cadena;

		public ObjetoComparableAuxiliar(int pEntero, String pCadena)
		{
			entero = pEntero;
			cadena = pCadena;
		}

		@Override
		public int compareTo(ObjetoComparableAuxiliar o) {

			if(entero > o.entero)
			{
				return 1;
			}
			else if(entero < o.entero)
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}

		public int darEntero()
		{
			return entero;
		}
		public String darCadena()
		{
			return cadena;
		}

	}

}	

