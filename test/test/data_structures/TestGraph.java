package test.data_structures;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileReader;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import com.opencsv.CSVReader;

import model.data_structures.Bag;
import model.data_structures.Graph;
import model.data_structures.IGrafo;
import model.data_structures.WeighedEdge;

public class TestGraph {

	public final static String ruta = "./test/dataTest/testNodes.csv";
	
	private Graph<NodoDeTest> grafo;
	private NodoDeTest sample;
	
	@Before
	public void setup ()
	{
		grafo = new Graph<NodoDeTest>(12);
		cargar(ruta); //Test de addVertex
	}

	@Test
	public void testV() {
		assertEquals(12, grafo.V());
	}
	
	@Test
	public void testE() {
		assertEquals(0, grafo.E());
	}

	@Test
	public void addEdge() {
		grafo.addEdge(1,3,5);
		Iterable<WeighedEdge> bag = grafo.adj(1);
		WeighedEdge edge = (WeighedEdge)bag.iterator().next();
		assertEquals(5,edge.weight(),0.3);
	}

	@Test
	public void getInfoVertex() {
		assertEquals(sample.darId(), grafo.getInfoVertex(10).darId());
	}

	@Test
	public void setInfoVertex() {
		grafo.setInfoVertex(1,sample);
		assertEquals(sample.darId(), grafo.getInfoVertex(1).darId());

	}

	@Test
	public void getCostArc() {
		grafo.addEdge(3, 5, 67);
		assertEquals(67,grafo.getCostArc(3, 5),0.1);
	}

	@Test
	public void setCostArc() {
		grafo.addEdge(3, 5, 0);
		assertEquals(0,grafo.getCostArc(3, 5),0.1);
		grafo.setCostArc(3, 5, 4.5);
		assertEquals(4.5,grafo.getCostArc(3, 5),0.1);
	}

	@Test
	public void adj() {
		grafo.addEdge(3, 5, 0);
		grafo.addEdge(3, 4, 0);
		grafo.addEdge(3, 2, 0);
		Bag<WeighedEdge> bag = (Bag<WeighedEdge>) grafo.adj(3);
		Iterator<WeighedEdge> iter = bag.iterator();
		while(iter.hasNext())
			assertEquals(0, iter.next().weight(),0.1);
	}

//	@Override
//	public void uncheck() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void dfs(int s) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public int cc() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	@Override
//	public Iterable getCC(int idVertex) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	
	private void cargar(String pRutaArchivo) {
		try
		{
			int contador =0;
			CSVReader reader = new CSVReader(new FileReader(new File(pRutaArchivo)));
			for(String[] linea : reader)
			{
				contador++;
				int id = contador; // Integer.parseInt(linea[0],10);
				NodoDeTest nodo = new NodoDeTest(id,
						linea[1],
						linea[2]);
				grafo.addVertex((int)id, nodo);
				sample = nodo;
			}
			reader.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	
}
