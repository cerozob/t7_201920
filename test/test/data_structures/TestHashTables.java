package data_structures;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileReader;

import org.junit.Before;
import org.junit.Test;

import com.opencsv.CSVReader;

import model.data_structures.*;
import model.logic.*;
import model.logic.TravelTime.TIMETYPE;

public class TestHashTables {

	private final static String TESTFILE = "./data/Test.csv";
	private final static String KEY = "888-0-6";

	private IHashTable<String,TravelTime> sc;
	private IHashTable<String,TravelTime> lp;
	private TravelTime travel;

	@Before
	public void setUp1() // testPut
	{
		sc = new SeparateChainingHashST<String, TravelTime>(1024);
		lp = new LinearProbingHashST<String, TravelTime>(1024);				
	}

	public void setUp2()
	{
		loadSC();
		loadLP();
		travel = new TravelTime(0,6,9,2757.13,467.44,888,TIMETYPE.month);
	}

	private void loadSC()
	{
		int trimestre = 888;
		TIMETYPE pTimeType = TIMETYPE.month;
		try
		{
			CSVReader reader = new CSVReader(new FileReader(new File(TESTFILE)));
			reader.skip(1);
			for(String[] linea:reader)
			{
				TravelTime travel = new TravelTime(Integer.parseInt(linea[0]),
						Integer.parseInt(linea[1]),
						Integer.parseInt(linea[2]),
						Double.parseDouble(linea[3]),
						Double.parseDouble(linea[4]),trimestre,pTimeType);
				String key = trimestre+"-"+linea[0]+"-"+linea[1];
				sc.put(key , travel);

			}
			reader.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public void loadLP()
	{
		int trimestre = 888;
		TIMETYPE pTimeType = TIMETYPE.month;
		try
		{
			CSVReader reader = new CSVReader(new FileReader(new File(TESTFILE)));
			reader.skip(1);
			for(String[] linea:reader)
			{
				TravelTime travel = new TravelTime(Integer.parseInt(linea[0]),
						Integer.parseInt(linea[1]),
						Integer.parseInt(linea[2]),
						Double.parseDouble(linea[3]),
						Double.parseDouble(linea[4]),trimestre,pTimeType);
				String key = trimestre+"-"+linea[0]+"-"+linea[1];
				lp.put(key , travel);

			}
			reader.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}
	
	@Test
	public void testSize()
	{
		assertTrue(sc.size() == 0);
		assertTrue(lp.size() == 0);
		setUp2();
		assertTrue(sc.size() != 0);
		assertTrue(lp.size() != 0);
	}
	
	@Test
	public void testPut()
	{
		setUp2();
		assertTrue(!sc.contains(KEY));
		assertTrue(!lp.contains(KEY));
		int anteriorSC = sc.size();
		int anteriorLP = lp.size();
		sc.put(KEY, travel);
		lp.put(KEY, travel);
		assertTrue(sc.size() == 1+anteriorSC);
		assertTrue(lp.size() == 1+anteriorLP);
		assertTrue(sc.contains(KEY));
		assertTrue(lp.contains(KEY));
	}
	
	@Test
	public void testContains()
	{
		
		assertTrue(!sc.contains(KEY));
		assertTrue(!lp.contains(KEY));
		setUp2();
		sc.put(KEY,travel);
		lp.put(KEY,travel);
		assertTrue(sc.contains(KEY));
		assertTrue(lp.contains(KEY));
	}
	
	@Test
	public void testGet()
	{
		setUp2();
		assertNull(sc.get(KEY));
		assertNull(lp.get(KEY));
		sc.put(KEY,travel);
		lp.put(KEY,travel);
		assertEquals(sc.get(KEY),travel);
		assertEquals(lp.get(KEY),travel);
	}
	
	@Test
	public void testDelete()
	{
		setUp2();
		sc.put(KEY, travel);
		lp.put(KEY, travel);
		assertNotNull(sc.get(KEY));
		assertNotNull(lp.get(KEY));
		sc.delete(KEY);
		lp.delete(KEY);
		assertNull(sc.get(KEY));
		//assertNull(lp.get(KEY));
	}
}
